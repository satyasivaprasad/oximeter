package com.m.myoximeter.data;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class ZLibUtils {
    public static byte[] compress(byte[] data) {
        byte[] output;
        byte[] bArr = new byte[0];
        Deflater compresser = new Deflater();
        compresser.reset();
        compresser.setInput(data);
        compresser.finish();
        ByteArrayOutputStream bos = new ByteArrayOutputStream(data.length);
        try {
            byte[] buf = new byte[1024];
            while (!compresser.finished()) {
                bos.write(buf, 0, compresser.deflate(buf));
            }
            output = bos.toByteArray();
        } catch (Exception e2) {
            output = data;
            e2.printStackTrace();
        } finally {
            try {
                bos.close();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
        }
        compresser.end();
        return output;
    }

    public static byte[] decompress(byte[] data) {
        byte[] output;
        byte[] bArr = new byte[0];
        Inflater decompresser = new Inflater();
        decompresser.reset();
        decompresser.setInput(data);
        ByteArrayOutputStream o = new ByteArrayOutputStream(data.length);
        try {
            byte[] buf = new byte[1024];
            while (!decompresser.finished()) {
                o.write(buf, 0, decompresser.inflate(buf));
            }
            output = o.toByteArray();
        } catch (Exception e2) {
            output = data;
            e2.printStackTrace();
        } finally {
            try {
                o.close();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
        }
        decompresser.end();
        return output;
    }
}
