package com.m.myoximeter.data;

import java.math.BigDecimal;

public class UnitUtils {
    public static final float BL2KG = 0.4535924f;
    public static final float CM2IN = 0.3937008f;
    public static final float IN2CM = 2.54f;
    public static final float KG2BL = 2.2046225f;

    public static float F2C(float f) {
        return FloatFormat((f - 32.0f) / 1.8f);
    }

    public static float C2F(float c) {
        return FloatFormat((1.8f * c) + 32.0f);
    }

    public static String TwoReserved(int num) {
        if (num <= 9) {
            return "0" + num;
        }
        return num + "";
    }

    public static float Kg2Bl(float num) {
        return 2.2046225f * num;
    }

    public static float Bl2kg(float num) {
        return 0.4535924f * num;
    }

    public static float FloatFormat(float num) {
        return new BigDecimal((double) num).setScale(2, 4).floatValue();
    }

    public static float FloatFormat3(float num) {
        return new BigDecimal((double) num).setScale(3, 4).floatValue();
    }

    public static float FloatFormat1(float num) {
        return new BigDecimal((double) num).setScale(1, 4).floatValue();
    }

    public static int Float2Int(float num) {
        return (int) (0.5f + num);
    }

    public static float Cm2In(float num) {
        return 0.3937008f * num;
    }

    public static float In2Cm(float num) {
        return 2.54f * num;
    }
}
