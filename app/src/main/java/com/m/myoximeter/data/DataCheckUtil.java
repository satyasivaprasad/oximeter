package com.m.myoximeter.data;

import android.content.Context;
import android.widget.Toast;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataCheckUtil extends Toast {
    public DataCheckUtil(Context context) {
        super(context);
    }

    public static boolean checkData(String data) {
        if (!Pattern.compile("^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$", 2).matcher(data).matches()) {
            return false;
        }
        return true;
    }

    public static boolean checkUri(String data) {
        if (!Pattern.compile("((http|https|ftp|rtsp|mms):(\\/\\/|\\\\\\\\){1}(([\\w-])+[.]){1,3}(net|com|cn|org|cc|tv|[0-9]{1,3})(\\S*\\/)((\\S)+[.]{1}(html|swf|jsp|php|jpg|gif|bmp|png)))", 2).matcher(data).matches()) {
            return false;
        }
        return true;
    }

    public static boolean checkMainData(String data, int min, int max) {
        if (Pattern.compile("^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{" + min + "," + max + "}$", 2).matcher(data).matches()) {
            return true;
        }
        return false;
    }

    public static boolean checkMainData2(String data) {
        if (Pattern.compile("^[A-Za-z0-9]+$", 2).matcher(data).matches()) {
            return true;
        }
        return false;
    }

    public static boolean checkBloodPressureArea(String data) {
        Pattern pattern = Pattern.compile("1\\d\\d", 2);
        Pattern pattern1 = Pattern.compile("[1-9]\\d", 2);
        Matcher matcher = pattern.matcher(data);
        Matcher matcher1 = pattern1.matcher(data);
        if (matcher.matches() || matcher1.matches()) {
            return true;
        }
        return false;
    }

    public static boolean checkBloodSugarArea(String data) {
        Pattern pattern5 = Pattern.compile("3[0-2]\\.\\d", 2);
        Pattern pattern4 = Pattern.compile("33\\.[0]", 2);
        Pattern pattern3 = Pattern.compile("3[0-3]{1}", 2);
        Pattern pattern = Pattern.compile("[1-2]{1}\\d", 2);
        Pattern pattern0 = Pattern.compile("[1-2]{1}\\d\\.\\d", 2);
        Pattern pattern1 = Pattern.compile("[0-9].\\d", 2);
        Pattern pattern2 = Pattern.compile("[1-9]{1}", 2);
        Matcher matcher = pattern.matcher(data);
        Matcher matcher0 = pattern0.matcher(data);
        Matcher matcher1 = pattern1.matcher(data);
        Matcher matcher2 = pattern2.matcher(data);
        Matcher matcher3 = pattern3.matcher(data);
        Matcher matcher4 = pattern4.matcher(data);
        Matcher matcher5 = pattern5.matcher(data);
        if (matcher.matches() || matcher0.matches() || matcher1.matches() || matcher2.matches() || matcher3.matches() || matcher4.matches() || matcher5.matches()) {
            return true;
        }
        return false;
    }

    public static String replaceNull(String str) {
        return str;
    }

    public static String consertTo(String str) {
        return str;
    }

    public static String stringFormat(String url) {
        String temp = url.replaceAll("/(.*)\r\n|\n|\r/g", "aa");
        if (url.contains("%")) {
            temp = Pattern.compile("\\%").matcher(temp).replaceAll("%25");
        }
        if (url.contains("#")) {
            temp = Pattern.compile("\\#").matcher(temp).replaceAll("%23");
        }
        if (url.contains("\"")) {
            temp = Pattern.compile("\\\"").matcher(temp).replaceAll("%22");
        }
        if (url.contains("\\")) {
            temp = Pattern.compile("\\\\").matcher(temp).replaceAll("%5C");
        }
        if (url.contains("+")) {
            temp = Pattern.compile("\\+").matcher(temp).replaceAll("%2B");
        }
        if (url.contains("^")) {
            temp = Pattern.compile("\\^").matcher(temp).replaceAll("%5E");
        }
        if (url.contains("`")) {
            temp = Pattern.compile("\\`").matcher(temp).replaceAll("%60");
        }
        if (url.contains("<")) {
            temp = Pattern.compile("\\<").matcher(temp).replaceAll("%3C");
        }
        if (url.contains(">")) {
            temp = Pattern.compile("\\>").matcher(temp).replaceAll("%3E");
        }
        String[] str = temp.split("\\?");
        if (str.length <= 1) {
            return temp;
        }
        return str[0] + "?" + temp.substring(str[0].length() + 1).replaceAll(" ", "+").replaceAll("　", "+");
    }

    public static String removeNull(String str) {
        return str.replace("null", "");
    }

    public static String convertTo(String str) {
        if (str.contains("&")) {
            return Pattern.compile("\\&").matcher(str).replaceAll("%26");
        }
        return str;
    }

    public static boolean isNull(String string) {
        if (string == null || "".equals(string) || "null".equals(string) || "无".equals(string)) {
            return true;
        }
        return false;
    }

    public static String isNullRePlace(String string) {
        return ("全部".equals(string) || string == null || "".equals(string) || "null".equals(string)) ? "" : string;
    }

    public static boolean StringFilter(String str) {
        return Pattern.compile("[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]").matcher(str).find();
    }

    public static boolean StringFilter2(String str) {
        return Pattern.compile("[']").matcher(str).find();
    }

    public static boolean isNumeric(String str) {
        if (!Pattern.compile("[0-9]*").matcher(str).matches()) {
            return false;
        }
        return true;
    }

    public static void showToast(String message, Context context) {
        Toast.makeText(context, message, 0).show();
    }

    public static void showToast(int messageId, Context context) {
        Toast.makeText(context, messageId, 0).show();
    }

    public static String resolveBleMsg(String hexStr) {
        String targetStr;
        String currentResult = "";
        for (int i = 4; i * 2 < hexStr.length(); i++) {
            String str = "";
            if ((i * 2) + 2 > hexStr.length()) {
                targetStr = hexStr.substring(i * 2, hexStr.length());
            } else {
                targetStr = hexStr.substring(i * 2, (i * 2) + 2);
            }
            int value = Integer.valueOf(String.valueOf(StringUtil.strtoul(targetStr, 16))).intValue();
            if (((char) value) == 0) {
                break;
            }
            currentResult = currentResult.concat(((char) value) + "");
        }
        return currentResult;
    }
}
