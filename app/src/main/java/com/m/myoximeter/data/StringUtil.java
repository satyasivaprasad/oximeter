package com.m.myoximeter.data;

import android.util.Base64;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.regex.Pattern;

public class StringUtil {
    public static String checkString(String string) {
        String[] temp = string.split("/(.*)\r\n|\n|\r/g");
        StringBuffer resoult = new StringBuffer();
        if (temp.length > 1) {
            resoult.append("<div class='section_content'>");
            for (String str : temp) {
                if (Pattern.matches("^\\d.*?$", str)) {
                    resoult.append("<div class='section_title'><p>" + str + "<p></div>");
                } else if (str.startsWith("●")) {
                    resoult.append("<div class='section_step'><p>" + str + "<p></div>");
                } else {
                    resoult.append("<div class='section_info'><p>" + str + "<p></div>");
                }
            }
            resoult.append("</div>");
        } else {
            resoult.append("<div class='section_info'>");
            resoult.append("<p>");
            resoult.append(string);
            resoult.append("</p>");
            resoult.append("</div>");
        }
        return resoult.toString();
    }

    public static String getTimeName() {
        Date date = new Date(System.currentTimeMillis());
        return new SimpleDateFormat("yyyyMMdd_hhmmssSSS").format(date) + "_" + new Random().nextInt(99999);
    }

    public static String getTimeNameSql() {
        DecimalFormat df = new DecimalFormat("#000");
        Date date = new Date(System.currentTimeMillis());
        return new SimpleDateFormat("yyMMddhhmmssSSS").format(date) + df.format((long) new Random().nextInt(999));
    }

    public static int[] getIntByString(String str) {
        String[] bgStrs = str.split("\\.");
        int[] bgs = new int[bgStrs.length];
        for (int i = 0; i < bgStrs.length; i++) {
            bgs[i] = Integer.valueOf(bgStrs[i]).intValue();
        }
        return bgs;
    }

    public static boolean isContainChinese(String str) {
        int i = 0;
        while (i < str.length()) {
            try {
                if (isChineseChar(str.charAt(i))) {
                    return true;
                }
                i++;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    private static boolean isChineseChar(char c) throws UnsupportedEncodingException {
        return String.valueOf(c).getBytes("GBK").length > 1;
    }

    public static String replaceBlank(String str) {
        String dest = "";
        if (str != null) {
            return Pattern.compile("\\s*|\t|\r|\n").matcher(str).replaceAll("");
        }
        return dest;
    }

    public static String getCompressString(String value) {
        return replaceBlank(new String(Base64.encode(ZLibUtils.compress(value.getBytes()), 0)));
    }

    public static String getDeCompressSting(String value) {
        return new String(ZLibUtils.decompress(Base64.decode(value.getBytes(), 0)));
    }

    public static boolean containsEmoji(String source) {
        int len = source.length();
        for (int i = 0; i < len; i++) {
            if (!isEmojiCharacter(source.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    private static boolean isEmojiCharacter(char codePoint) {
        return codePoint == 0 || codePoint == 9 || codePoint == 10 || codePoint == 13 || (codePoint >= ' ' && codePoint <= 55295) || ((codePoint >= 57344 && codePoint <= 65533) || (codePoint >= 0 && codePoint <= 65535));
    }

    public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (byte b : src) {
            String hv = Integer.toHexString(b & 255);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    public static long strtoul(String str, int base) {
        return strtoul((str + "\u0000").toCharArray(), base);
    }

    private static long strtoul(char[] cp, int base) {
        long result = 0;
        int i = 0;
        if (base == 0) {
            base = 10;
            if (cp[0] == '0') {
                base = 8;
                i = 0 + 1;
                if (Character.toLowerCase(cp[i]) == 'x' && isxdigit(cp[1])) {
                    i++;
                    base = 16;
                }
            }
        } else if (base == 16 && cp[0] == '0' && Character.toLowerCase(cp[1]) == 'x') {
            i = 0 + 2;
        }
        while (isxdigit(cp[i])) {
            long value = isdigit(cp[i]) ? (long) (cp[i] - '0') : (long) ((Character.toLowerCase(cp[i]) - 'a') + 10);
            if (value >= ((long) base)) {
                break;
            }
            result = (((long) base) * result) + value;
            i++;
        }
        return result;
    }

    private static boolean isxdigit(char c) {
        return ('0' <= c && c <= '9') || ('a' <= c && c <= 'f') || ('A' <= c && c <= 'F');
    }

    private static boolean isdigit(char c) {
        return '0' <= c && c <= '9';
    }
}
