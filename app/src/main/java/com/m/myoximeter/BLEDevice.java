package com.m.myoximeter;

import java.io.Serializable;

public class BLEDevice implements Serializable {
    private String MainVersion = "";
    private String SNumber;
    private String SubVersion = "";
    private String address;
    private String name;
    private String type;

    public BLEDevice(String n, String sn, String add, String typ) {
        this.name = n;
        this.SNumber = sn;
        this.address = add;
        this.type = typ;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address2) {
        this.address = address2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getSNumber() {
        return this.SNumber;
    }

    public void setSNumber(String SNumber2) {
        this.SNumber = SNumber2;
    }

    public String getMainVersion() {
        return this.MainVersion;
    }

    public void setMainVersion(String mainVersion) {
        this.MainVersion = mainVersion;
    }

    public String getSubVersion() {
        return this.SubVersion;
    }

    public void setSubVersion(String subVersion) {
        this.SubVersion = subVersion;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type2) {
        this.type = type2;
    }
}
