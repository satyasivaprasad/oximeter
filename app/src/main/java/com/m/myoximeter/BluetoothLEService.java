package com.m.myoximeter;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.util.List;
import java.util.UUID;
import java.util.Vector;

import static com.m.myoximeter.Spo2RenderingScreen.oxiMeterDataHandler;

public class BluetoothLEService extends Service {

    public static final String TAG = BluetoothLEService.class.getSimpleName();
    public static final UUID UUID_BLUETOOTHLE_CHARACTERISTIC = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    private final IBinder mIBinder = new LocalBinder();
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;
    private BluetoothManager mBluetoothManager;
    private BluetoothGattCharacteristic mCharacteristic;
    private static final String UUID_BLUETOOTH_CHARACTERISTIC = "0000ffe1-0000-1000-8000-00805f9b34fb";
    private static final String UUID_BLUETOOTH_SERVICE = "0000ffe0-0000-1000-8000-00805f9b34fb";

    public final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            broadcastUpdate(characteristic);
        }

        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == 0) {
                broadcastUpdate(characteristic);
            }
        }

        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.d(BluetoothLEService.TAG, "============status: " + status);
            if (status == 133) {
                Log.i(BluetoothLEService.TAG, "Failed to connect.");
            } else if (newState == 2) {
                Log.i(BluetoothLEService.TAG, "Connected to GATT server.");
                Log.i(BluetoothLEService.TAG, "Attempting to start service discovery: " + BluetoothLEService.this.mBluetoothGatt.discoverServices());
            } else if (newState == 0) {
                Log.i(BluetoothLEService.TAG, "Disconnected from GATT server.");
            }
            super.onConnectionStateChange(gatt, status, newState);
        }

        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == 0) {
                Log.i(BluetoothLEService.TAG, "ACTION_GATT_SERVICES_DISCOVERED");
                displayGattServices(getSupportedGattService());
            } else {
                Log.w(BluetoothLEService.TAG, "onServicesDiscovered received: " + status);
            }
        }

        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Log.d(BluetoothLEService.TAG, "----------write success---------status: " + status);
        }

        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorRead(gatt, descriptor, status);
        }

        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            Log.i(BluetoothLEService.TAG, "onDescriptorWrite = " + status + ", descriptor = " + descriptor.getUuid().toString());
        }

        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            Log.i(BluetoothLEService.TAG, "rssi = " + rssi);
        }

        public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
            super.onReliableWriteCompleted(gatt, status);
        }
    };

    public IBinder onBind(Intent intent) {
        return this.mIBinder;
    }

    public boolean onUnbind(Intent intent) {
        close();
        return super.onUnbind(intent);
    }

    public boolean initialize() {
        if (this.mBluetoothManager == null) {
            this.mBluetoothManager = (BluetoothManager) getSystemService("bluetooth");
            if (this.mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }
        this.mBluetoothAdapter = this.mBluetoothManager.getAdapter();
        if (this.mBluetoothAdapter != null) {
            return true;
        }
        Log.e(TAG, "Unable to initialize BluetoothAdapter.");
        return false;
    }

    public boolean connect(String address) {
        if (this.mBluetoothAdapter == null || address == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        } else if (this.mBluetoothDeviceAddress == null || !address.equals(this.mBluetoothDeviceAddress) || this.mBluetoothGatt == null) {
            BluetoothDevice device = this.mBluetoothAdapter.getRemoteDevice(address);
            if (device == null) {
                Log.w(TAG, "Device not found. Unable to connect.");
                return false;
            }
            this.mBluetoothGatt = device.connectGatt(this, false, this.mGattCallback);
            Log.d(TAG, "Trying to create a new connection.");
            this.mBluetoothDeviceAddress = address;
            return true;
        } else {
            Log.d(TAG, "Trying to use an existing BluetoothGatt for connection.");
            if (!this.mBluetoothGatt.connect()) {
                return false;
            }
            return true;
        }
    }

    public void disconnect() {
        if (this.mBluetoothAdapter == null || this.mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
        } else {
            this.mBluetoothGatt.disconnect();
        }
    }

    public void close() {
        if (this.mBluetoothGatt != null) {
            this.mBluetoothGatt.close();
            this.mBluetoothGatt = null;
        }
    }

    public void broadcastUpdate(BluetoothGattCharacteristic characteristic) {
        byte[] data = characteristic.getValue();
        int length = data.length;
        if (length > 0) {
            StringBuilder stringBuilder = new StringBuilder(length);
            for (byte byteChar : data) {
                stringBuilder.append(String.format("%02X", new Object[]{Byte.valueOf(byteChar)}));
            }
            oxiMeterDataHandler.parseData(stringBuilder.toString());
        } else {
            Log.e(TAG, "No Data Available");
        }
    }

    public void writeCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (this.mBluetoothAdapter == null || this.mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
        } else {
            this.mBluetoothGatt.writeCharacteristic(characteristic);
        }
    }

    public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (this.mBluetoothAdapter == null || this.mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
        } else {
            this.mBluetoothGatt.readCharacteristic(characteristic);
        }
    }

    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic, boolean enabled) {
        if (this.mBluetoothAdapter == null || this.mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        this.mBluetoothGatt.setCharacteristicNotification(characteristic, enabled);
        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID_BLUETOOTHLE_CHARACTERISTIC);
        if (descriptor != null) {
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            this.mBluetoothGatt.writeDescriptor(descriptor);
        }
    }

    public List<BluetoothGattService> getSupportedGattService() {
        if (this.mBluetoothGatt == null) {
            return null;
        }
        return this.mBluetoothGatt.getServices();
    }

    public boolean getRssiVal() {
        if (this.mBluetoothGatt == null) {
            return false;
        }
        return this.mBluetoothGatt.readRemoteRssi();
    }

    public class LocalBinder extends Binder {
        public LocalBinder() {
        }

        public BluetoothLEService getService() {
            return BluetoothLEService.this;
        }
    }

    public void displayGattServices(List<BluetoothGattService> gattServices) {
        if (gattServices != null) {
            for (BluetoothGattService gattService : gattServices) {
                String ServiceUUID = gattService.getUuid().toString();
                if (ServiceUUID.equals(UUID_BLUETOOTH_SERVICE)) {
                    Log.i(TAG, "Service UUID is : " + ServiceUUID);
                    for (BluetoothGattCharacteristic gattCharacteristic : gattService.getCharacteristics()) {
                        if (gattCharacteristic.getUuid().toString().equals(UUID_BLUETOOTH_CHARACTERISTIC)) {
                            Log.i(TAG, "Characteristic UUID is : " + ServiceUUID);
                            this.mCharacteristic = gattCharacteristic;
                            int a = this.mCharacteristic.getProperties();
                            int b = this.mCharacteristic.getPermissions();
                            System.out.println("getProperties is : " + a);
                            System.out.println("getPermissions is : " + b);
                            setCharacteristicNotification(gattCharacteristic, true);
                            gattCharacteristic.setValue("send data->");
                            writeCharacteristic(gattCharacteristic);
                        }
                    }
                }
            }
        }
    }
}
