package com.m.myoximeter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;


public class OximetersListAdapter extends RecyclerView.Adapter<OximetersListAdapter.ViewHolder> {

    OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;
    private List<BLEDevice> mLeDevices;
    private Activity mContext;

    public OximetersListAdapter(Activity c, List<BLEDevice> showDevice) {
        this.mLeDevices = showDevice;
        this.mContext = c;
    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.onRecyclerViewItemClickListener = listener;
    }

    @NonNull
    @Override
    public OximetersListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.oxi_meters_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final BLEDevice bleDevice = mLeDevices.get(position);
        holder.deviceAddress.setText(bleDevice.getAddress());
        holder.deviceName.setText(bleDevice.getName());
        holder.type.setText(bleDevice.getType());
        holder.sn.setText(bleDevice.getSNumber());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onRecyclerViewItemClickListener.onRecyclerViewItemClicked(bleDevice.getAddress());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mLeDevices.size();
    }

    public void clear() {
        int size = mLeDevices.size();
        mLeDevices.clear();
        notifyItemRangeRemoved(0, size);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView deviceAddress;
        public TextView deviceName;
        public TextView type;
        public TextView sn;

        public ViewHolder(View itemView) {
            super(itemView);
            this.deviceAddress = (TextView) itemView.findViewById(R.id.device_address);
            this.deviceName = (TextView) itemView.findViewById(R.id.device_name);
            this.type = (TextView) itemView.findViewById(R.id.type);
            this.sn = (TextView) itemView.findViewById(R.id.sn);
        }
    }
}
