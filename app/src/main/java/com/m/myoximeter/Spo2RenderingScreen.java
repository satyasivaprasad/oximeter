package com.m.myoximeter;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.SurfaceHolder;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.Vector;

public class Spo2RenderingScreen extends AppCompatActivity {

    public static final String TAG = Spo2RenderingScreen.class.getSimpleName();
    public static OxiMeterDataHandler oxiMeterDataHandler = new OxiMeterDataHandler();
    public BluetoothLEService mBluetoothLEService;
    public boolean isBindService = false;
    private String selectedAddress;
    public final ServiceConnection mServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLEService = ((BluetoothLEService.LocalBinder) service).getService();
            if (!mBluetoothLEService.initialize()) {
                Log.e(MainActivity.TAG, "Unable to initialize Bluetooth!");
            } else {
                Log.d(MainActivity.TAG, "Initialize Bluetooth successfully.");
            }
            Log.d(MainActivity.TAG, "Try to connect.");
            mBluetoothLEService.connect(selectedAddress);

            mSPO2WaveView.startDraw();
        }

        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLEService = null;
        }
    };
    private SPO2WaveView mSPO2WaveView;
    private Vector<Integer> SPO2WaveValues;
    private Vector<Integer> PIValues;
    private TextView spo2, heart_rate;

    private void startService(String deviceId) {
        isBindService = bindService(new Intent(Spo2RenderingScreen.this, BluetoothLEService.class), mServiceConnection, 1);
        if (isBindService) {
            Log.d(MainActivity.TAG, "Bind Service successfully");
        } else {
            Log.w(MainActivity.TAG, "Fail to bind service!");
        }
        if (mBluetoothLEService != null) {
            Log.d(MainActivity.TAG, "Connect request result = " + mBluetoothLEService.connect(deviceId));
        }
    }

    public <T> T $(int id) {
        return (T) findViewById(id);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spo2_rendering_screen);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        SPO2WaveValues = new Vector<Integer>();
        PIValues = new Vector<Integer>();
        mSPO2WaveView = (SPO2WaveView) findViewById(R.id.SPO2Wave);
        mSPO2WaveView.setValues(SPO2WaveValues);
        mSPO2WaveView.setPIValues(PIValues);
        mSPO2WaveView.setZOrderOnTop(true);    // necessary
        SurfaceHolder sfhTrackHolder = mSPO2WaveView.getHolder();
        sfhTrackHolder.setFormat(PixelFormat.TRANSPARENT);

        spo2 = $(R.id.spo2);
        heart_rate = $(R.id.heart_rate);

        selectedAddress = getIntent().getStringExtra("selectedAddress");

        startService(selectedAddress);

        oxiMeterDataHandler.setOnM70cDataListener(new OxiMeterDataHandler.onDataUpdatedListener() {
            @Override
            public void setSPO2Value(final int var1) {
                Log.v(TAG, "SPO2 value " + var1);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spo2.setText("" + var1);
                    }
                });
            }

            @Override
            public void setHeartRateValue(final int var1) {
                Log.v(TAG, "HeartRate value " + var1);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        heart_rate.setText("" + var1);
                    }
                });
            }

            @Override
            public void setPI(Float var1) {
                Log.v(TAG, "PI value " + var1);

            }

            @Override
            public void setRespValue(int var1) {
                Log.v(TAG, "RespValue value " + var1);
            }

            @Override
            public void setBattery(String var1) {
                Log.v(TAG, "Battery value " + var1);
            }

            @Override
            public void setMain(String var1) {
                Log.v(TAG, "Main value " + var1);
            }

            @Override
            public void setSub(String var1) {
                Log.v(TAG, "Sub value " + var1);
            }

            @Override
            public void setSPo2ValuesPIValues(final Vector<Integer> var1, final Vector<Integer> var2) {
                Log.v(TAG, "SPo2ValuesPIValues value " + var1 + " var2 " + var2);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSPO2WaveView.setValues(var1);
                        mSPO2WaveView.setPIValues(var2);
                    }
                });
            }
        });
    }

}