package com.m.myoximeter;

public interface OxiMeterM70cData {
    void parseData(String var1);
    void setOnM70cDataListener(OxiMeterDataHandler.onDataUpdatedListener var1);
}
