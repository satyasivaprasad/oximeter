package com.m.myoximeter;

public interface OnRecyclerViewItemClickListener {
    public void onRecyclerViewItemClicked(String address);
}
