package com.m.myoximeter;

import android.util.Log;

import java.util.Vector;

public class OxiMeterDataHandler implements OxiMeterM70cData {

    private String main;
    private String sub;
    private String battery;
    private String receivedData = "";
    private boolean isFirstReceive = true;
    private int historyCounter = 0;
    private Vector<Integer> SPO2WaveValues = new Vector();
    private Vector<Integer> PIValues = new Vector();
    private onDataUpdatedListener onDataUpdatedListener;

    @Override
    public void parseData(String rawData) {
        String head = rawData.substring(0, 2);
        String data = rawData.replace(" ", "");
        if (data.contains("AAAA")) {
            data = data.replaceAll("AAAA", "AA");
        }

        if (head.equals("AA") & data.length() > 4) {
            String dataID = data.substring(4, 6);
            if (dataID.equals("41")) {
                Log.e("TAG", "received data values is : " + this.receivedData);
                ++this.historyCounter;
                if (this.historyCounter == 10) {
                    this.historyCounter = 0;
                }

                if (!this.isFirstReceive) {
                    int SPO2Value = Integer.parseInt(this.receivedData.substring(72, 74), 16) & 127;
                    Log.e("spo2  value", "" + SPO2Value);
                    this.onDataUpdatedListener.setSPO2Value(SPO2Value);
                    int heartRateValue = Integer.parseInt(this.receivedData.substring(74, 76), 16);
                    Log.e("heartRateValue  value", "" + heartRateValue);
                    this.onDataUpdatedListener.setHeartRateValue(heartRateValue);

                    int respValue;
                    for (int i = 0; i < 30; ++i) {
                        respValue = Integer.parseInt(this.receivedData.substring(10 + i * 2, 12 + i * 2), 16);
                        this.PIValues.add(respValue);
                    }

                    float PI = (float) Integer.parseInt(this.receivedData.substring(70, 72), 16) / 10.0F;
                    Log.e("PI", "" + PI);
                    this.onDataUpdatedListener.setPI(PI);
                    respValue = Integer.parseInt(this.receivedData.substring(76, 78), 16);
                    Log.e("respValue", "" + respValue);
                    this.onDataUpdatedListener.setRespValue(respValue);
                    short[] r = new short[]{(short) ((short) (Short.parseShort(this.receivedData.substring(80, 82), 16) << 8) + Short.parseShort(this.receivedData.substring(78, 80), 16)), (short) ((short) (Short.parseShort(this.receivedData.substring(84, 86), 16) << 8) + Short.parseShort(this.receivedData.substring(82, 84), 16)), (short) ((short) (Short.parseShort(this.receivedData.substring(88, 90), 16) << 8) + Short.parseShort(this.receivedData.substring(86, 88), 16)), (short) ((short) (Short.parseShort(this.receivedData.substring(92, 94), 16) << 8) + Short.parseShort(this.receivedData.substring(90, 92), 16))};
                    Log.e("the value of short", "r0" + r[0] + "r1" + r[1] + "r2" + r[2] + "r3" + r[3] + "");
                    String wave = this.receivedData.substring(94, this.receivedData.length() - 2);
                    Log.e("the values of waves??", wave);

                    for (int i = 0; i < wave.length(); i += 2) {
                        String value = wave.substring(i, i + 2);
                        int v = Integer.parseInt(value, 16) & 127;
                        this.SPO2WaveValues.add(v);
                    }

                    Log.d("TAG", "Values number is : " + this.SPO2WaveValues.size());
                    this.onDataUpdatedListener.setSPo2ValuesPIValues(this.SPO2WaveValues, this.PIValues);
                    this.receivedData = data;
                }

                this.receivedData = data;
                this.isFirstReceive = false;
            } else if (dataID.equals("43")) {
                Log.d("ll", "battery state is :" + data.substring(6, 8));
                this.main = data.substring(8, 9) + "." + data.substring(9, 10);
                this.sub = data.substring(10, 11) + "." + data.substring(11, 12);
                this.battery = data.substring(6, 8);
            }

            this.onDataUpdatedListener.setMain(this.main);
            this.onDataUpdatedListener.setSub(this.sub);
            this.onDataUpdatedListener.setBattery(this.battery);
        } else {
            this.receivedData = this.receivedData + data;
            Log.e("to see the final receivedData", "" + this.receivedData);
        }
    }

    @Override
    public void setOnM70cDataListener(OxiMeterDataHandler.onDataUpdatedListener var1) {
        this.onDataUpdatedListener = var1;
    }

    public interface onDataUpdatedListener {
        void setSPO2Value(int var1);

        void setHeartRateValue(int var1);

        void setPI(Float var1);

        void setRespValue(int var1);

        void setBattery(String var1);

        void setMain(String var1);

        void setSub(String var1);

        void setSPo2ValuesPIValues(Vector<Integer> var1, Vector<Integer> var2);
    }
}
