package com.m.myoximeter;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothManager;

public class BluetoothDeviceManager {

    private BluetoothAdapter bluetoothAdapter;
    private String bluetoothDeviceAddress;
    private BluetoothGatt bluetoothGatt;
    private BluetoothManager bluetoothManager;
    private BluetoothGattCharacteristic gattCharacteristic;
    private static final String UUID_BLUETOOTH_CHARACTERISTIC = "0000ffe1-0000-1000-8000-00805f9b34fb";
    private static final String UUID_BLUETOOTH_SERVICE = "0000ffe0-0000-1000-8000-00805f9b34fb";


}
