package com.m.myoximeter

import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.m.myoximeter.data.DataCheckUtil
import com.m.myoximeter.data.StringUtil
import com.m.myoximeter.extension.hasPermission
import com.m.myoximeter.extension.toast
import no.nordicsemi.android.support.v18.scanner.BluetoothLeScannerCompat
import no.nordicsemi.android.support.v18.scanner.ScanFilter
import no.nordicsemi.android.support.v18.scanner.ScanSettings
import java.util.*

class MainActivity : AppCompatActivity() {
    var mHandler: Handler? = null
    private val bluetoothDevicesScanningMonitor = Handler()

    var deviceList: MutableList<BLEDevice?> = ArrayList()
    var isScanning = false
    var mBluetoothAdapter: BluetoothAdapter? = null
    var isBluetoothON = false
    var isHasPermissions = true
    var isConnecting = false
    var bleDevicesListView: RecyclerView? = null
    var progressBar: ProgressBar? = null
    var oximetersListAdapter: OximetersListAdapter? = null
    private var isSupportBluetooth = true
    private var BLETimer: Timer? = null
    private var BLETimerTask: TimerTask? = null
    private val scanCallback: ScanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            super.onScanResult(callbackType, result)
            mHandler!!.post {
                val bluetoothDevice = result.device
                Log.v(TAG, "Device name " + bluetoothDevice.name)
                val scanRecord = result.scanRecord
                val scanRecordBytes = scanRecord!!.bytes
                val deviceName = "BLT_M70C"
                if (bluetoothDevice.name != null && bluetoothDevice.name == deviceName) {
                    progressBar!!.setVisibility(View.GONE)
                    val type = scanRecordBytes[12]
                    var typeS = ""
                    Log.d(TAG, "type is $type")
                    val hexString = StringUtil.bytesToHexString(scanRecordBytes)
                    var sn: String? = ""
                    if (hexString != null && hexString.length > 50) {
                        sn = DataCheckUtil.resolveBleMsg(hexString.substring(18, 50))
                    }
                    var hasSameAddress = isSameAddress(bluetoothDevice)
                    if (!hasSameAddress) {
                        if (type.toInt() == 48) {
                            typeS = "M70C"
                        } else if (type.toInt() == 1) {
                            typeS = "WT1"
                        }
                        deviceList.add(BLEDevice(bluetoothDevice.name, sn, bluetoothDevice.address, typeS))
                        Log.d(TAG, "add device")
                        val message = Message()
                        message.what = 2
                        mHandler!!.sendMessage(message)
                    }
                }
            }
        }

        override fun onBatchScanResults(results: List<ScanResult>) {
            super.onBatchScanResults(results)
        }

        override fun onScanFailed(errorCode: Int) {
            super.onScanFailed(errorCode)
        }
    }

    private fun isSameAddress(bluetoothDevice: BluetoothDevice): Boolean {
        var hasSameAddress = false
        val it: Iterator<*> = deviceList.iterator()
        while (true) {
            if (it.hasNext()) {
                if ((it.next() as BLEDevice).address == bluetoothDevice.address) {
                    hasSameAddress = true
                    break
                }
            } else {
                break
            }
        }
        return hasSameAddress
    }

    /* access modifiers changed from: private */
    fun scanDevice(enable: Boolean) {
        Log.v(TAG, "Start Scanning")
        val bluetoothLeScanner = mBluetoothAdapter!!.bluetoothLeScanner
        if (enable) {
            progressBar!!.setVisibility(View.VISIBLE)
            isScanning = true
            bluetoothLeScanner.startScan(scanCallback)
            startReScanMonitor()
            return
        }
        isScanning = false
        bluetoothLeScanner.stopScan(scanCallback)
    }

    private val bluetoothDiscoveryResult = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.action == BluetoothDevice.ACTION_FOUND) {
                val bluetoothDevice: BluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)!!
                val deviceName = "BLT_M70C"
                if (bluetoothDevice.name != null && bluetoothDevice.name == deviceName) {
                    progressBar!!.setVisibility(View.GONE)
                    var hasSameAddress = isSameAddress(bluetoothDevice)
                    if (!hasSameAddress) {
                        deviceList.add(BLEDevice(bluetoothDevice.name, bluetoothDevice.address, bluetoothDevice.address, bluetoothDevice.address))
                        Log.d(TAG, "add device")
                        val message = Message()
                        message.what = 2
                        mHandler!!.sendMessage(message)
                    }
                }
            }
        }
    }

    /* Broadcast receiver to listen for discovery updates. */
    private val bluetoothDiscoveryMonitor = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                BluetoothAdapter.ACTION_DISCOVERY_STARTED -> {
                    Log.v(TAG, "Start Discovery")
                }
                BluetoothAdapter.ACTION_DISCOVERY_FINISHED -> {
                    Log.v(TAG, "Discovery Completed")
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bleDevicesListView = findViewById(R.id.devices_list_view)

        progressBar = findViewById(R.id.progress_bar)
        restart()
    }

    private val enableReScanUI: Runnable = Runnable {
        startDiscovery()
    }

    fun startReScanMonitor() {
        bluetoothDevicesScanningMonitor.postDelayed(enableReScanUI, 10000)
    }

    private fun restart() {
        initializeAdapter()
        initHandler()
        bLEPermissions
        setTimer()
        deviceList = ArrayList<BLEDevice?>()
        if (isHasPermissions) {
            isHasPermissions = true
            initBluetooth()
        }
    }

    private fun initializeAdapter() {
        oximetersListAdapter = OximetersListAdapter(this, deviceList)
        bleDevicesListView!!.layoutManager = LinearLayoutManager(this)
        bleDevicesListView!!.adapter = oximetersListAdapter
        oximetersListAdapter!!.setOnRecyclerViewItemClickListener { address ->
            val intent = Intent(this@MainActivity, Spo2RenderingScreen::class.java)
            intent.putExtra("selectedAddress", address)
            startActivity(intent)
        }
    }

    private fun setTimer() {
        BLETimer = Timer()
        BLETimerTask = object : TimerTask() {
            override fun run() {
                if (mBluetoothAdapter != null) {
                    isBluetoothON = mBluetoothAdapter!!.isEnabled
                }
                if (!isBluetoothON) {
                    val message = Message()
                    message.what = 0
                    mHandler!!.sendMessage(message)
                    if (isScanning) {
                        scanDevice(false)
                    }
                    isScanning = false
                } else if (!isScanning && !isConnecting) {
                    mBluetoothAdapter = (this@MainActivity.getSystemService("bluetooth") as BluetoothManager).adapter
                    isScanning = true
                    mHandler!!.postDelayed({ scanDevice(true) }, 1000)
                    val message2 = Message()
                    message2.what = 5
                    mHandler!!.sendMessage(message2)
                }
            }
        }
        BLETimer!!.schedule(BLETimerTask, 0, 100)
        mBluetoothAdapter = (this@MainActivity.getSystemService("bluetooth") as BluetoothManager).adapter
        isScanning = true
        mHandler!!.postDelayed({ scanDevice(true) }, 1000)
    }

    private val bLEPermissions: Unit
        private get() {
            if (ContextCompat.checkSelfPermission(this, "android.permission.ACCESS_COARSE_LOCATION") != 0) {
                isHasPermissions = false
                ActivityCompat.requestPermissions(this, arrayOf("android.permission.ACCESS_COARSE_LOCATION"), 0)
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, "android.permission.READ_CONTACTS")) {
                    Toast.makeText(this, "shouldShowRequestPermissionRationale", 0).show()
                }
            }
        }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        isHasPermissions = grantResults[0] != -1
        startActivity(Intent(this@MainActivity, MainActivity::class.java))
        //        finish();
    }

    private fun initBluetooth() {
        mBluetoothAdapter = (getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager).adapter
        checkBluetooth()
        checkBLE()
        checkBluetooth()
        if (mBluetoothAdapter != null) {
            isBluetoothON = mBluetoothAdapter!!.isEnabled
        }
        if (!isBluetoothON) {
            mBluetoothAdapter!!.enable()
        }
        setTimer()
    }

    private fun checkBluetooth() {
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Not support Bluetooth!", 0).show()
        }
        isSupportBluetooth = false
    }

    private fun checkBLE() {
        if (!packageManager.hasSystemFeature("android.hardware.bluetooth_le")) {
            Toast.makeText(this, "Not support BLE!", 1).show()
        }
        isSupportBluetooth = false
    }

    private fun initHandler() {
        mHandler = Handler(Handler.Callback { message ->
            when (message.what) {
                0 -> {
                }
                1 -> {
                }
                2 -> {
                    Log.v(TAG, "devices list size " + deviceList.size)
                    initializeAdapter()
                }
                4 -> {
                }
                5 -> {
                }
                12 -> {
                }
            }
            true
        })
    }

    private fun startDiscovery() {
        Log.v(TAG, "Start Discovery")
        if (hasPermission(Manifest.permission.ACCESS_COARSE_LOCATION)) {
            if (mBluetoothAdapter!!.isEnabled && !mBluetoothAdapter!!.isDiscovering) {
                beginDiscovery()
            }
        }
    }

    private fun beginDiscovery() {
        registerReceiver(bluetoothDiscoveryResult, IntentFilter(BluetoothDevice.ACTION_FOUND))
        monitorDiscovery()
        mBluetoothAdapter!!.startDiscovery()
    }

    private fun monitorDiscovery() {
        registerReceiver(bluetoothDiscoveryMonitor, IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED))
        registerReceiver(bluetoothDiscoveryMonitor, IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED))
    }

    companion object {
        @JvmField
        val TAG = MainActivity::class.java.simpleName
    }
}